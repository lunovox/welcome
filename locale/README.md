# HOW TO TRANSLATE

To generate the '**template.pot**' file (if it does not exist), use the terminal command:

````
cd welcome
xgettext -n *.lua -L Lua --force-po --keyword=modWelcome.translate --from-code=UTF-8 -o ./locale/template.pot

````
---------------------------------------------

### Create '.po' file to translate (if it does not exist) from '.pot' template file.

Sintaxe:
```
msginit --no-translator --no-wrap --locale=$LANG.UTF-8 --output-file=$LANG.po --input=$POT
```
Example to brasilian portuguese language (pt_BR):
```
msginit --no-translator --no-wrap --locale=pt_BR.UTF-8 --output-file=./locale/pt_BR.po --input=./locale/template.pot
```
---------------------------------------------

### Update '.po' file to translate from '.pot' template file.

Sintaxe:
```
msgmerge --sort-output --no-wrap --update --backup=off $LANG.po $POT
```
Example to brasilian portuguese language (pt_BR):
```
msgmerge --sort-output --no-wrap --update --backup=off ./locale/pt_BR.po ./locale/template.pot
```
---------------------------------------------

To edit '**.po**' files to your language use app poedit.:

Example of translation into brasilian portuguese language (pt_BR):
````
cd welcome
sudo apt-get inatall poedit
poedit ./locale/pt_BR.po
````

* See others locales used: ca;cs;da;de;dv;eo;es;et;fr;hu;id;it;ja;jbo;kn;lt;ms;nb;nl;pl;pt;pt_BR;ro;ru;sl;sr_Cyrl;sv;sw;tr;uk

---------------------------------------------

## Convert '.po' file to '.tr' file.

The '.tr' file is used as translations up to version '5.9.1'. Only from version '5.10' was support for translation via '.po' file implemented.

````
cd ./locale/
lua po2tr.lua "welcome" "pt_BR.po"; mv "pt_BR.tr" "welcome.pt_BR.tr"
cat welcome.pt_BR.tr | less
````
---------------------------------------------

### TO ENABLE YOUR LANGUAGE IN MINETEST/LUANTI

* For Sample: To enable the translate to brasilian portuguese language, write ```language = pt_BR``` in file "minetest.conf". Or write the command ```/set -n language pt_BR``` in game chat, and run again the minetest game.

---------------------------------------------

## SUBMITTING YOUR TRANSLATION

If you understand any language other than English, please consider submitting your language translation '.po' file to '<lunovox@disroot.org>'!

![](../screenshot.png)

---------------------------------------------

> See more: 
* https://forum.minetest.net/viewtopic.php?f=47&t=21974
* https://github.com/minetest/minetest/issues/8158
* https://gist.githubusercontent.com/mamchenkov/3690981/raw/8ebd48c2af20c893c164e8d5245d9450ad682104/update_translations.sh
* https://gitlab.com/4w/xtend/-/blob/master/xtend_default/tools/convert_po_file_to_tr_file/convert_po_file_to_tr_file.lua


