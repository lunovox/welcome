ATTENTION: On the server <SERVERNAME> any player who breaks these rules will be SEVERELY punished according to the administrator's wishes.

 ### JUSTICE:

    #01 - Do not use hacks or other programs that give you exclusive advantages!  (The punishments for this infraction will be very severe)

 ### BEHAVIOR:

    #02 - All dialogues are recorded.  Anything you say that breaks the rules can and will be used against you.

    #03 - Do not bother other players, and especially the 'Admin', with harmful requests.  Reason: No one is obliged to give you anything.  This server is not in 'creative' mode and you are solely responsible for reading the tips in this "welcome" menu to be able to provide your own materials.

    #04 - Do not invite players to play on other servers, neither publicly nor privately, using the resources of this server.  This hurts the mod developers who voluntarily work to increase the attractiveness of this server.

    #05 - Do not enter into dialogue with the Admin, unless to discuss a matter of interest to the server's public.  Reason: The Admin is a player (it is a maintenance account).  Only contact him if absolutely necessary!

    #06 - People who practice hueragem (corruption and terrorism attacking other players) can be punished (harmed) by other citizens (other players)!
   
    #07 - Reports about inflicted rules must be made via email addressed to <brazucas@disroot.org>.  Or by 'letter' (game item), and addressed to the 'Admin' with a description of the date-time and location of the occurrence.

 ### LAND PROTECTION:
   
    #08 - The land (including the chests and doors in the location) belongs for a limited, renewable time to the citizen (the player) who protects the land (Except on the roads and shoulders connected to the spawn).
   
    #09 - The owner of the land can share (or unshare) access to interaction with his land (and also the chests) with other people at his own discretion!
   
    #10 - Digging (digging holes, mining) is only allowed on your own protected land, or in the public mines below spawn, or in remote unprotected locations away from other players' homes.  (Avoid making the landscape ugly!)
   
    #11 - Every citizen must place a sign with their email address for contact in front of their house!  The Admin will use your email address if necessary to communicate something urgent of interest to you (for example, recover your password).  Other forms of contact are allowed, but only email is mandatory as it is a free and accessible software technology.

 ### VEHICLES:

    #12 - It is prohibited to park your vehicle (of any type) for more than 30 minutes on public roads or on private land that belongs to other players.  Penalty is the seizure of the vehicle by the police and a vehicle fine of 05 minercash (type of money acquired when playing).  The police must communicate by letter after seizing the vehicle.

    #13 - Within 30 real business days after the seizure notice, if the player does not pay the vehicle fine, and/or does not recover his vehicle.  The amount of the fine will be subtracted from a 'Disposal of Possession' auction and the remaining money will be deposited into the vehicle's former owner's bank account.