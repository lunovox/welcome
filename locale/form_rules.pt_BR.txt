
ATENÇÃO: No servidor <SERVERNAME> qualquer jogador que quebrar estas regras sera punido SEVERAMENTE conforme a vontade do administrador.

### JUSTIÇA:

   #01 - Não utilize hack ou outros programas que lhe derem vantagens exclusivas! (As punições nesta infração serão severíssimas)

### COMPORTAMENTO:

   #02 - Todos os diálogos são gravados. Tudo o que você disser que infligir as regras poderá e será usada contra você.

   #03 - Não incomode outros jogadores, e principalmente ao 'Admin', com pedidos perniciosos. Motivo: Ninguém tem a obrigação de lhe dar nada. Este servidor não está em modo 'creative' e você é o unico responsável de ler as dicas nesse menu de "boas vindas" para adquirir condições de prover seus próprios materiais.

   #04 - Não convide os jogadores para jogarem em outros servidores, nem publicamente e nem em privado, utilizando os recursos deste servidor. Isso magoa os desenvolvedores de mods que trabalham voluntariamente para aumentar a atratividade deste servidor.

   #05 - Não entre em dialogo com o Admin, a não ser para tratar de assunto de interesse do público do servidor. Motivo: O Admin é um jogador (é uma conta de manutenção). Somente entre em contato com ele em caso de extrema necessidade!

   #06 - Pessoas que praticam hueragem (corrupção e terrorismos atacando outros jogadores) podem ser punidas (judiadas) por outros cidadãos (outros jogadores)!
   
   #07 - Denúncias sobre regras infligidas devem ser feitas através de e-mail endereçado ao <brazucas@disroot.org>. Ou por 'carta' (item do jogo), e endereçadas ao 'Admin' com descrição de data-hora e local da ocorrência. 

### PROTEÇÃO DE TERRENO:
   
   #08 - O terreno (incluindo os baús e portas no local) pertencem por tempo limitado renovável ao cidadão (ao jogador) que proteger o terreno (Exceto sobre as estradas e acostamentos ligados ao spawn).
   
   #09 - O dono do terreno pode compartilhar (ou descompartilhar) o acesso a interação com seu terreno (e também dos baús) com outras pessoas conforme sua própria vontade!
   
   #10 - Só é permitido escavar (fazer buracos, minerar) em seu próprio terreno protegido, ou nas minas públicas embaixo do spawn, ou em locais remotos desprotegidos longes das casas de outros jogadores. (Evite deixar feia a paisagem!)
   
   #11 - Cada cidadão deve por uma placa com seu endereço de e-mail para contato na frente de sua casa! O Admin utilizará seu endereço de e-mail caso seja necessário para lhe comunicar algo urgente de seu interesse (por exemplo recuperar senha de acesso). Outras formas de contato são permitidas, mas apenas e-mail é obrigatório por ser uma tecnologia de software livre e acessível.

### VEÍCULOS:

   #12 - É proibido manter estacionado por mais de 30 minutos o seu veículo (de qualquer tipo) em estradas públicas ou em terrenos privados que pertença a outros jogadores. Penalidade é de apreensão de veículo no pálio da polícia e multa-veicular de 05 minercash (tipo de dinheiro adquirido ao jogar). A polícia deve obrigatoriamente comunicar por carta após a apreensão do veículo.

   #13 - Nos 30 dias úteis reais após a comunicação de apreensão, caso o jogador não pague a multa-veicular, e/ou não venha recuperar seu veículo. O valor da multa será subtraído de um leilão de 'Alienação de Posse' e o restante do dinheiro será depositado na conta bancária do ex-proprietário do veículo.
   
   
