

if core.global_exists("modComputing") then
   modComputing.add_app("welcome:btnShowNewpaper", {
   	icon_name = "btnShowNewpaper",
   	icon_title = modWelcome.translate("WELCOME"),
   	icon_descryption = modWelcome.translate(
   	   "Displays a window with the latest @n"
   	   .."newspaper news. Along with buttons @n"
   	   .."for: Rules, Story, Hints."
   	),
   	icon_type = "button", --types: button/button_exit
   	icon_image = "icon_newspaper.1.png",
   	on_iconclick = function(player)
   	   local playername = player:get_player_name()
   	   core.log(
			   "action","[WELCOME] "
			   ..modComputing.translate(
               "Player '@1' is trying to access the '@2' via the '@3'!"
               , playername
               , "Newpaper"
               , "computing app"
            )
         )
         local fileLanguage = modWelcome.getFileLanguage(player)
			core.show_formspec(playername, "frmWelcome", modWelcome.getFormspecIndex(player, fileLanguage))
   	end,
   })
end