

modWelcome.doNormalize = function(playername, text)
	if type(text)=="nil" then
		text = ""
	end
	local servername1 = core.settings:get("welcome.servername")
	local servername2 = core.settings:get("server_name")
	if servername1 ~= nil 
		and type(servername1) == "string"
		and servername1 ~= ""
	then
		text = text:gsub('<servername>', servername1)
		text = text:gsub('<SERVERNAME>', servername1:upper())
	elseif servername2 ~= nil 
		and type(servername2) == "string"
		and servername2 ~= ""
	then
		text = text:gsub('<servername>', servername2)
		text = text:gsub('<SERVERNAME>', servername2:upper())
	else
		text = text:gsub('<servername>', modWelcome.servername)
		text = text:gsub('<SERVERNAME>', modWelcome.servername:upper())
	end
	
	
	local serverurl = core.settings:get("server_url")
	if serverurl ~= nil 
		and type(serverurl) == "string"
		and serverurl ~= ""
	then
		text = text:gsub('<serverurl>', serverurl)
	else
		text = text:gsub('<serverurl>', modWelcome.serverurl)
	end
	
	local serveraddress = core.settings:get("server_address")
	if serveraddress ~= nil 
		and type(serveraddress) == "string"
		and serveraddress ~= ""
	then
		text = text:gsub('<serveraddress>', serveraddress)
	else
		text = text:gsub('<serveraddress>', modWelcome.serveraddress)
	end
	
	local serverport = core.settings:get("port")
	if serverport ~= nil 
		and type(serverport) == "string"
		and serverport ~= ""
	then
		text = text:gsub('<serverport>', serverport)
	else
		text = text:gsub('<serverport>', modWelcome.serverport)
	end
	
	if type(playername)=="string" and playername~="" then
		text = text:gsub('<playername>', playername)
		
		local player = core.get_player_by_name(playername)
		if player and player:is_player() then
			local lang_code = modWelcome.getFileLanguage(player)
			text = text:gsub('<lang_code>', lang_code)
		end
	end
	
	return text
end

modWelcome.isLanguage = function(language)
	local content = modWelcome.getFileContent("locale/form_rules."..language..".txt")
	return (type(content) == "string" and content ~= "")
end

function modWelcome.file_exists(filename)
   if type(filename)=="string" and filename~="" then
	   local handler = io.open(filename,"r")
	   if handler ~= nil then 
	   	io.close(handler) 
	   	return true 
		end
   end
	return false
end

modWelcome.getFileLanguage = function(player)
	if player and player:is_player() then
		local playername = player:get_player_name()
		local playerinfo = core.get_player_information(playername)
		local playerlang = playerinfo.lang_code
		if type(playerlang)=="string" 
			and playerlang~=""
			and modWelcome.file_exists(
				modWelcome.modpath.."/newpaper/form_jornal."..playerlang..".html"
			) 
		then
			return playerlang
		end
	end
	
	local language = core.settings:get("language")
	if language ~= nil 
		and type(language) == "string"
		and language ~= ""
		and modWelcome.isLanguage(language)
	then
		return language
	else
		return "template"
	end
end

modWelcome.getFileContent = function(myFile)
	local handler = io.open(modWelcome.modpath.."/"..myFile , "rb")
	if handler then
		local content = handler:read("*all")
		handler:close()
		return content
	end
end

modWelcome.debug = function(testo, playername)
	local isDebug = core.settings:get_bool("welcome.debug") --Default: false
	if isDebug then
		if playername ~= nil and type(playername) == "string" and playername ~= "" then
			core.chat_send_player(playername, testo)
		else
			core.chat_send_all(testo)
		end
	end
end

modWelcome.logOpenWindows = function(playername, windowname)
	core.log('action',"[WELCOME] "..("The player '%s' opened '%s window'."):format(playername, windowname))
end

core.register_on_player_receive_fields(function(player, formname, fields)
	local playername = player:get_player_name()
	modWelcome.debug(
		"core.register_on_player_receive_fields >>> "
		.."player = "..dump(playername)
		.." | formname = "..dump(formname)
		.." | fields = "..dump(fields)
	, playername)
	
	if formname == "frmWelcome" then -- This is your form name
		local fileLanguage = modWelcome.getFileLanguage(player)
		if fields.btnBack~=nil then
			modWelcome.logOpenWindows(playername, "Index")
			core.show_formspec(playername, "frmWelcome", modWelcome.getFormspecIndex(player, fileLanguage))
		elseif fields.btnRules~=nil or fields.myBrowser == "action:lnkRules" then
			modWelcome.logOpenWindows(playername, "Rules")
			--core.show_formspec(playername, "frmWelcome", modWelcome.getFormspecRules())
			core.show_formspec(playername, "frmWelcome", 
				--modWelcome.getFormspecHints()
				modWelcome.getFormspecDefault(
					modWelcome.translate("Rules for this Server:"),
					modWelcome.doNormalize(playername, 
						modWelcome.getFileContent("locale/form_rules."..fileLanguage..".txt")
					)
				)
			)
		elseif fields.btnStory~=nil then
			modWelcome.logOpenWindows(playername, "Story")
			--core.show_formspec(playername, "frmWelcome", modWelcome.getFormspecStory(playername))
			core.show_formspec(playername, "frmWelcome", 
				--modWelcome.getFormspecHints()
				modWelcome.getFormspecDefault(
					modWelcome.translate("The world story:"),
					modWelcome.doNormalize(playername, 
						modWelcome.getFileContent("locale/form_story."..fileLanguage..".txt")
					)
				)
			)
		elseif fields.btnHints~=nil then
			modWelcome.logOpenWindows(playername, "Hints")
			core.show_formspec(playername, "frmWelcome", 
				--modWelcome.getFormspecHints()
				modWelcome.getFormspecDefault(
					modWelcome.translate("Hints on how to play:"),
					modWelcome.doNormalize(playername, 
						modWelcome.getFileContent("locale/form_hints."..fileLanguage..".txt")
					)
				)
			)
		end
	end
end)

modWelcome.getPropCommWelcome = function()
	return {
		params = "",
		description = modWelcome.translate("Displays Welcome panel with: Story, Rules, and Hints."),
		privs = {},
		func = function(playername, param)
			local player = core.get_player_by_name(playername)
			if type(player)~="nil" and player:is_player() then
				local fileLanguage = modWelcome.getFileLanguage(player)
				modWelcome.logOpenWindows(playername, "Index")
				core.show_formspec(playername, "frmWelcome", modWelcome.getFormspecIndex(player, fileLanguage))
			end
		end,
	}
end

core.register_on_joinplayer(function(player)
	local delay = core.settings:get("welcome.delay")
	local playername = player:get_player_name()
	local fileLanguage = modWelcome.getFileLanguage(player)
	if delay ~= nil 
		and type(tonumber(delay)) == "number"
		and tonumber(delay) >= 1
	then
		core.after(tonumber(delay), function()
			local playername = player:get_player_name()
			core.show_formspec(playername, "frmWelcome", modWelcome.getFormspecIndex(player, fileLanguage))
		end)
	else
		local playername = player:get_player_name()
		core.show_formspec(playername, "frmWelcome", modWelcome.getFormspecIndex(player, fileLanguage))
	end
end)


