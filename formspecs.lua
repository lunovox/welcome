modWelcome.getFormspecIndex = function(player)
	local fileLanguage = modWelcome.getFileLanguage(player)
	local playername = player:get_player_name()
	local body = modWelcome.doNormalize(playername, 
		modWelcome.getFileContent("newpaper/form_jornal."..fileLanguage..".html")
	)
	local formspec = "size[11.0,7.50]"
		.."bgcolor[#636D76FF;false]"
		--.."bgcolor[#00880044;false]"
		--.."background[0,0;3.0,2.75;craftopoles.png]"
		--.. default.gui_bg
		--.. default.gui_bg_img
		--.."image[0,0;3.6,0.5;welcome_servertitle.png]"
		.."box[0.0,-0.1;10.75,0.6;#000088]"
		.."label[0.25,0;"..
			minetest.formspec_escape(
				core.get_color_escape_sequence("#FFFFFF")
				..modWelcome.translate("WELCOME NEWSPAPER")
			)
		.."]"
		
		.."box[0,0.75;10.75,6.00;#000000CC]"
		.."hypertext[0.5,1.00;10.50,6.50;myBrowser;"
		--..minetest.formspec_escape("<global margin=10 valign=0 color=#FF00FF hovercolor=#00FFFF size=12 font=normal halign=center >")
		.."<global valign=middle halign=left margin=15 background=#00000000 color=#FFFFFF hovercolor=#8888FF size=18 font=normal>"
		.."<tag name=html   color=#FFFFFF>"
		.."<tag name=body   color=#FFFFFF>"
		.."<tag name=p      background=#888888 color=#FFFFFF size=14 halign=justify margin=15>"
		
		.."<tag name=red     color=#FF0000>"
		.."<tag name=green   color=#00FF00>"
		.."<tag name=blue    color=#0000FF>"
		.."<tag name=cyan    color=#88CCFF>"
		.."<tag name=yellow  color=#FFFF00>"
		.."<tag name=magenta color=#FF00FF>"
		.."<tag name=pink    color=#FF88FF>"
		.."<tag name=orange  color=#FF8800>"
		.."<tag name=brown   color=#964B00>"
		.."<tag name=black   color=#000000>"
		.."<tag name=grey    color=#888888>"
		.."<tag name=white   color=#FFFFFF>"
		
		.."<tag name=code   color=#88CC88 size=16 font=mono>"
		.."<tag name=hidden color=#000000 size=10 font=mono>"
		
		.."<tag name=action color=#88CCFF hovercolor=#FFFF00>"
		..minetest.formspec_escape(body)
		.."]" -- Fim de hypertext[]
		--.."container_end[]"
		
		.."button[0.00,7.00;2.50,0.5;btnStory;"..minetest.formspec_escape(modWelcome.translate("Story")).."]"
		.."button[2.50,7.00;2.50,0.5;btnRules;"..minetest.formspec_escape(modWelcome.translate("Rules")).."]"
		.."button[5.00,7.00;2.50,0.5;btnHints;"..minetest.formspec_escape(modWelcome.translate("Hints")).."]"
		.."button_exit[8.50,7.00;2.50,0.5;;"..minetest.formspec_escape(modWelcome.translate("Close")).."]"
	return formspec
end

modWelcome.getFormspecDefault = function(label, body)
	local formspec = "size[10,7.75]"
		.."bgcolor[#00880044;false]"
		.."image[0,0;3.6,0.5;welcome_servertitle.png]"
		.."box[0,0.65;9.75,6.6;#000000CC]"
		.."label[0.1,0.75;"..minetest.formspec_escape(core.colorize("#00FFFF", label)).."]"
		.."textarea[0.5,1.25;9.5,6.75;;;"..minetest.formspec_escape(body).."]"
		.."button[7.75,7.5;2,0.5;btnBack;"..minetest.formspec_escape(modWelcome.translate("Back")).."]"
	return formspec
end