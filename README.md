
![welcome_title]
# [WELCOME][download_repo_link]

[![download_minetest_icon]][download_minetest_link] [![download_stable_icon]][download_stable_link] [![download_repo_icon]][download_repo_link]

Displays a window with the latest newspaper news. Along with buttons for: Rules, Story, Hints.

---------------------------------------

## **License:**

* [![license_code_icon]][license_code_link]
* [![license_media_icon]][license_media_link]

See more details in wiki: 
 
[![wiki_en_icon]][wiki_en_link] [![wiki_pt_icon]][wiki_pt_link]

---------------------------------------

## **Dependencies:**

| Mod Name | Dependency Type | Descryption |
| :--: | :--: | :-- |
| [computing][mod_computing] | Optional | Add multiple devices, such as mobile phones and desktops, that allow you to access the 'welcome newpaper' remotely. | 

---------------------------------------

## **Commands:**

* ````/bemvindo```` ou ````/welcome```` : Displays the 'Welcome' window! (The same one that opens automatically every time the player joins the server.)

## **Developers:**

* Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [xmpp](xmpp:lunovox@disroot.org?join), [social web](http://qoto.org/@lunovox), [audio conference](https://meet.jit.si/MinetestBrasil), [more contacts](https://libreplanet.org/wiki/User:Lunovox)

---------------------------------------

## **Settings:**

In **minetest.conf** file:

You don't need to do any of these presets mentioned below to make this mod work. But it's worth knowing the configuration values in case you want to directly change the ````minetest.conf```` file.

| Settings | Descryption |
| :-- | :-- |
| ````welcome.debug = <boolean>```` 	| If show debug info of this mod. Only util to developers. Default: ````false````. |
| ````welcome.delay = <number>```` 		| window with delay in seconds. Default: ````0````. Min: ````0````. |

---------------------------------------

[favicon]:https://gitlab.com/lunovox/welcome/-/raw/master/favicon.png
[welcome_title]:https://gitlab.com/lunovox/welcome/-/raw/master/textures/welcome_servertitle.png

[download_stable_icon]:https://img.shields.io/static/v1?label=Download&message=Mod&color=blue
[download_stable_link]:https://gitlab.com/lunovox/welcome/-/tags
[download_repo_icon]:https://img.shields.io/static/v1?label=Projeto&message=GIT&color=red
[download_repo_link]:https://gitlab.com/lunovox/welcome
[download_minetest_icon]:https://img.shields.io/static/v1?label=Minetest&message=Game&color=brightgreen
[download_minetest_link]:https://minetest.net

[mod_computing]:https://gitlab.com/lunovox/computing

[license_code_icon]:https://img.shields.io/static/v1?label=LICENSE%20CODE&message=GNU%20AGPL%20v3.0&color=yellow
[license_code_link]:https://gitlab.com/lunovox/minertrade/-/raw/welcome/LICENSE_CODE
[license_media_icon]:https://img.shields.io/static/v1?label=LICENSE%20MEDIA&message=CC%20BY-SA-4.0&color=yellow
[license_media_link]:https://gitlab.com/lunovox/minertrade/-/raw/welcome/LICENSE_MEDIA
[wiki_en_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=EN&color=blue
[wiki_en_link]:https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License
[wiki_pt_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=PT&color=blue
[wiki_pt_link]:https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License
