local ngettext

--[[
local S = minetest.get_translator('testmod')
minetest.register_craftitem('testmod:test', {
    description = S('I am a test object'),
    inventory_image = 'default_stick.png^[brighten'
})
--]]

if minetest.get_modpath("intllib") then
	if intllib.make_gettext_pair then
		-- New method using gettext.
		modWelcome.translate, ngettext = intllib.make_gettext_pair()
	else
		-- Old method using text files.
		modWelcome.translate = intllib.Getter()
	end
elseif minetest.get_translator ~= nil and minetest.get_modpath ~= nil and minetest.get_modpath(modWelcome.modname) then
	modWelcome.translate = function(s)
		local Trans = minetest.get_translator(modWelcome.modname)
		--Source: https://minetest.gitlab.io/minetest/escape-sequences/
		local texto = minetest.strip_background_colors(
			minetest.strip_colors(s)
		)
		return Trans(texto)
	end
else
	modWelcome.translate = function(s) 
		--core.get_color_escape_sequence
		local texto = minetest.strip_background_colors(
			minetest.strip_colors(s)
		)
		return texto
	end
end
