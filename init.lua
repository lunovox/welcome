modWelcome = { 
	modname = core.get_current_modname(),
	modpath = core.get_modpath(core.get_current_modname()),
	servername = "BRAZUCAS", --Nome simples e sem acento!
	serverurl = "https://qoto.org/tags/BRAZUCAS",
	serveraddress = "168.138.149.251",
	serverport = "30000",
}

dofile(modWelcome.modpath.."/translate.lua") -- <== Antes de 'api.lua'!
dofile(modWelcome.modpath.."/formspecs.lua")
dofile(modWelcome.modpath.."/api.lua")
dofile(modWelcome.modpath.."/commands.lua")
dofile(modWelcome.modpath.."/item_computing_app.lua")

core.log('action',"[MOD] "..modWelcome.modname.." loaded!")
